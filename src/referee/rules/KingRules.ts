import { posix } from 'path';
import { TeamType } from '../../Types';
import { Piece, Position } from '../../models';
import { tileIsEmptyOrOccupiedByOpponent, tileIsOccupied, tileIsOccupiedByOpponent } from "./GeneralRules";

export const kingMove = (
    initialPosition: Position,
    desiredPosition: Position,
    team: TeamType,
    boardState: Piece[]
): boolean => {
    for (let i = 1; i < 2; i++) {

        let multiplierX = (desiredPosition.x < initialPosition.x) ? -1 : (desiredPosition.x > initialPosition.x) ? 1 : 0;
        let multiplierY = (desiredPosition.y < initialPosition.y) ? -1 : (desiredPosition.y > initialPosition.y) ? 1 : 0;
        let passedPosition: Position = new Position(initialPosition.x + (i * multiplierX), initialPosition.y + (i * multiplierY));
        if (passedPosition.samePosition(desiredPosition)) {
            if (tileIsEmptyOrOccupiedByOpponent(passedPosition, boardState, team)) {
                return true;
            }
        } else {
            if (tileIsOccupied(passedPosition, boardState)) {
                break;
            }
        }
    }

    return false;
}

export const getPossibleKingMoves = (king: Piece, boardState: Piece[]): Position[] => {
    const possibleKingMoves: Position[] = [];


    // top movement
    for (let i = 1; i < 2; i++) {
        const destination = new Position(king.position.x, king.position.y + i)

        if (destination.x < 0 || destination.x > 7 || destination.y < 0 || destination.y > 7) {
            break;
        }

        if (!tileIsOccupied(destination, boardState)) {
            possibleKingMoves.push(destination);
        } else if (tileIsOccupiedByOpponent(destination, boardState, king.team)) {
            possibleKingMoves.push(destination);
            break;
        } else {
            break;
        }

    }

    // bottom movement
    for (let i = 1; i < 2; i++) {
        const destination = new Position(king.position.x, king.position.y - i);

        if (destination.x < 0 || destination.x > 7 || destination.y < 0 || destination.y > 7) {
            break;
        }

        if (!tileIsOccupied(destination, boardState)) {
            possibleKingMoves.push(destination);
        } else if (tileIsOccupiedByOpponent(destination, boardState, king.team)) {
            possibleKingMoves.push(destination);
            break;
        } else {
            break;
        }


    }
    // right movement
    for (let i = 1; i < 2; i++) {
        const destination = new Position(king.position.x + i, king.position.y);

        if (destination.x < 0 || destination.x > 7 || destination.y < 0 || destination.y > 7) {
            break;
        }

        if (!tileIsOccupied(destination, boardState)) {
            possibleKingMoves.push(destination);
        } else if (tileIsOccupiedByOpponent(destination, boardState, king.team)) {
            possibleKingMoves.push(destination);
            break;
        } else {
            break;
        }

    }
    // left  movement
    for (let i = 1; i < 2; i++) {
        const destination = new Position(king.position.x - i, king.position.y);

        if (destination.x < 0 || destination.x > 7 || destination.y < 0 || destination.y > 7) {
            break;
        }

        if (!tileIsOccupied(destination, boardState)) {
            possibleKingMoves.push(destination);
        } else if (tileIsOccupiedByOpponent(destination, boardState, king.team)) {
            possibleKingMoves.push(destination);
            break;
        } else {
            break;
        }

    }

    //upper right movement
    for (let i = 1; i < 2; i++) {
        const destination = new Position(king.position.x + i, king.position.y + i);

        if (destination.x < 0 || destination.x > 7 || destination.y < 0 || destination.y > 7) {
            break;
        }

        if (!tileIsOccupied(destination, boardState)) {
            possibleKingMoves.push(destination);
        } else if (tileIsOccupiedByOpponent(destination, boardState, king.team)) {
            possibleKingMoves.push(destination);
            break;
        } else {
            break;
        }
    }

    //bottom right movement
    for (let i = 1; i < 2; i++) {
        const destination = new Position(king.position.x + i, king.position.y - i);

        if (destination.x < 0 || destination.x > 7 || destination.y < 0 || destination.y > 7) {
            break;
        }

        if (!tileIsOccupied(destination, boardState)) {
            possibleKingMoves.push(destination);
        } else if (tileIsOccupiedByOpponent(destination, boardState, king.team)) {
            possibleKingMoves.push(destination);
            break;
        } else {
            break;
        }
    }

    //bottom left movement
    for (let i = 1; i < 2; i++) {
        const destination = new Position(king.position.x - i, king.position.y - i);

        if (destination.x < 0 || destination.x > 7 || destination.y < 0 || destination.y > 7) {
            break;
        }

        if (!tileIsOccupied(destination, boardState)) {
            possibleKingMoves.push(destination);
        } else if (tileIsOccupiedByOpponent(destination, boardState, king.team)) {
            possibleKingMoves.push(destination);
            break;
        } else {
            break;
        }
    }
    //upper left movement
    for (let i = 1; i < 2; i++) {
        const destination = new Position(king.position.x - i, king.position.y + i);

        if (destination.x < 0 || destination.x > 7 || destination.y < 0 || destination.y > 7) {
            break;
        }

        if (!tileIsOccupied(destination, boardState)) {
            possibleKingMoves.push(destination);
        } else if (tileIsOccupiedByOpponent(destination, boardState, king.team)) {
            possibleKingMoves.push(destination);
            break;
        } else {
            break;
        }
    }

    return possibleKingMoves;
}

export const getCastlingMoves = (king: Piece, boardState: Piece[]): Position[] => {
    const possibleMoves: Position[] = [];

    if (king.hasMoved) return possibleMoves;

    const rooks = boardState.filter(p => p.isRook && p.team === king.team && !p.hasMoved);

    for (const rook of rooks) {
        const direction = (rook.position.x - king.position.x > 0) ? 1 : -1;
        const adjacentPosition = king.position.clone();
        adjacentPosition.x += direction;

        if (!rook.possibleMoves?.some(m => m.samePosition(adjacentPosition))) continue;

        const concernedTiles = rook.possibleMoves.filter(m => m.y === king.position.y);

        const enemyPieces = boardState.filter(p => p.team !== king.team);

        let valid = true;

        for (const enemy of enemyPieces) {
            if (enemy.possibleMoves === undefined) continue;

            for (const move of enemy.possibleMoves) {
                if (concernedTiles.some(t => t.samePosition(move))) {
                    valid = false;
                }
                if (!valid)
                    break;
            }
            if (!valid)
                break;
        }

        if (!valid) continue;

        possibleMoves.push(rook.position.clone());

    }



    return possibleMoves;
}